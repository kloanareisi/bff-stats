import logging
import urllib.request
from modules import support
from modules import download
from modules import readInput
from modules import doAnalysis
from modules.user import User
from modules.player import Player

# global variables
listOfFiles = {'players.json':'https://api.sleeper.app/v1/players/nfl',
'users.json':'https://api.sleeper.app/v1/league/<league_id>/users',
'rosters.json':'https://api.sleeper.app/v1/league/<league_id>/rosters',
'matchup_<week_XX>.json':'https://api.sleeper.app/v1/league/<league_id>/matchups/<week_XX>',
'games.csv':'http://www.habitatring.com/games.csv'}
logger = logging.getLogger('logger')
#leagueId = str(723524283950751744)
leagueId = str(706898880163282944)
currentYear = 2021
users = {}
players = {}
weeks = []
matchups = []
medianStarterPoints = {}

def setupLogging():
	# create logger
	logger = logging.getLogger('logger')
	logger.setLevel(logging.DEBUG)
	fh = logging.FileHandler('log.log')
	fh.setLevel(logging.DEBUG)
	# create formatter and add it to the handlers
	formatter = logging.Formatter('%(asctime)s\t%(levelname)s\t%(message)s','%Y-%m-%d_%H:%M:%S')
	fh.setFormatter(formatter)
	# add the handlers to the logger
	logger.addHandler(fh)
	return logger
		
def main():
	# Setup logging
	# logger = setupLogging()
		
	# Download files
	download.downloadFiles(True, leagueId, listOfFiles, logger)
		
	# Read input
	readInput.readWeeks(weeks, currentYear)
	readInput.readUser(users)
	readInput.readRoster(users)
	readInput.readPlayer(players)
	readInput.readMatchup(weeks, matchups)
	doAnalysis.getPoints(users, matchups)
	doAnalysis.getRanks(weeks, users, matchups)
	doAnalysis.getStarterPointsAgainst(weeks, users, matchups)
	doAnalysis.getWinsLosses(weeks, users)
	doAnalysis.getWinsLossesDoubleHeader(weeks, users, medianStarterPoints)
	
	# Print output
	support.printOutput(weeks, users, medianStarterPoints)
if __name__ == "__main__":
    main()

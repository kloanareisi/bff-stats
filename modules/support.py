import os
import xlwt
from xlwt import Workbook

def printUsers(users):
	for u in users:
		print('rid: ' + str(users[u].getRosterId()) 
			+ ', uid:' + str(users[u].getId()) 
			+ ', name: ' + users[u].getName()
			# + ', wins: ' + str(users[u].getWins()) 
			# + ', ties: ' + str(users[u].getTies()) 
			# + ', losses: ' + str(users[u].getLosses())
			# + ', ppts: ' + str(users[u].getPpts()) 
			# + ', fpts: ' + str(users[u].getFpts())
			# + ', fptsAgainst: ' + str(users[u].getFptsAgainst())
			+ ', playerPoints: ' + str(users[u].getPlayerPoints())
			# + ', starterPoints: ' + str(users[u].getStarterPoints())
			# + ', players: ' + str(users[u].getPlayers())
			+ ', rank: ' + str(users[u].getRank())
			)
			
def printPlayers(players):
	for k,p in players.items():
		print('pid: ' + str(p.getId())
		+ ', name: ' + p.getName()
		+ ', team: ' + p.getTeam()
		+ ', pos: ' + str(p.getFantasyPositions())
		)
		
def printWeeks(weeks):
	print(str(weeks))
	
def printMatchups(matchups):
	for m in matchups:
		print(str(m.getWeek()) + ',' + str(m.getMatchupId()) + ',' + str(m.getRosterId()))
	
def printOutput(weeks, users, medianStarterPoints):
	wb = Workbook()
	style = xlwt.easyxf('font: bold 1')
	row = 0
	column = 0
	rank = []
	sheet = wb.add_sheet('Ranking')
	sheet.write(row, column, 'User', style)
	sheet.write(row, column + 1, 'SumStarterPoints', style)
	sheet.write(row, column + 2, 'W-L', style)
	row = row + 1
	for id,u in users.items():
		wins = 0
		for w in weeks:
			if w in u.getWinsLosses():
				if u.getWinsLosses()[w] == 'W':
					wins = wins + 1
		rank.append([u.getName(), wins, getWinsLossesString(u.getWinsLosses()), sum(u.getStarterPoints().values())])
	rankSorted = sorted(rank, key=lambda x: (x[1], x[3]), reverse=True)
	for i in rankSorted:
		sheet.write(row, column, i[0])
		sheet.write(row, column + 1, i[3])
		sheet.write(row, column + 2, i[2])
		row = row + 1
	
	row = 0
	column = 0
	rank = []
	rankSorted = []
	sheet = wb.add_sheet('DoubleHeaderRanking')
	sheet.write(row, column, 'User', style)
	sheet.write(row, column + 1, 'SumStarterPoints', style)
	sheet.write(row, column + 2, 'W-L', style)
	row = row + 1
	for id,u in users.items():
		wins = 0
		for w in weeks:
			if w in u.getWinsLosses():
				if u.getWinsLosses()[w] == 'W':
					wins = wins + 1
				if u.getWinsLossesDoubleHeader()[w] == 'W':
					wins = wins + 1
		rank.append([u.getName(), wins, getWinsLossesString(u.getWinsLosses(), u.getWinsLossesDoubleHeader()), sum(u.getStarterPoints().values())])
	rankSorted = sorted(rank, key=lambda x: (x[1], x[3]), reverse=True)
	for i in rankSorted:
		sheet.write(row, column, i[0])
		sheet.write(row, column + 1, i[3])
		sheet.write(row, column + 2, i[2])
		row = row + 1
	
	row = 0
	column = 0
	for id,u in users.items():
		sheet = wb.add_sheet(u.getName())
		sheet.write(row, column, 'Week', style)
		sheet.write(row, column + 1, 'StarterPoints', style)
		sheet.write(row, column + 2, 'Rank', style)
		sheet.write(row, column + 3, 'OpponentStarterPoints', style)
		sheet.write(row, column + 4, 'OpponentRank', style)
		sheet.write(row, column + 5, 'medianStarterPoints', style)
		sheet.write(row, column + 6, 'Win/Loss', style)
		sheet.write(row, column + 7, 'DoubleHeaderWin/Loss', style)
		row = 1
		column = 0
		for w in weeks:
			if w in u.getStarterPoints():
				sheet.write(row, column, w)
				sheet.write(row, column + 1, u.getStarterPoints()[w])
				sheet.write(row, column + 2, u.getRank()[w])
				sheet.write(row, column + 3, u.getOpponentStarterPoints()[w])
				sheet.write(row, column + 4, u.getOpponentRank()[w])
				sheet.write(row, column + 5, medianStarterPoints[w])
				sheet.write(row, column + 6, u.getWinsLosses()[w])
				sheet.write(row, column + 7, u.getWinsLossesDoubleHeader()[w])
				row = row + 1
				column = 0
		sheet.write(row, column, 'SUM', style)
		sheet.write(row, column + 1, sum(u.getStarterPoints().values()), style)
		sheet.write(row, column + 2, '-', style)
		sheet.write(row, column + 3, sum(u.getOpponentStarterPoints().values()), style)
		sheet.write(row, column + 4, '-', style)
		sheet.write(row, column + 5, sum(medianStarterPoints.values()), style)
		sheet.write(row, column + 6, getWinsLossesString(u.getWinsLosses()), style)
		sheet.write(row, column + 7, getWinsLossesString(u.getWinsLossesDoubleHeader()), style)
		
		row = row + 1
		column = 0
		sheet.write(row, column, 'AVG', style)
		sheet.write(row, column + 1, round(sum(u.getStarterPoints().values())/len(weeks), 2), style)
		sheet.write(row, column + 2, '-', style)
		sheet.write(row, column + 3, round(sum(u.getOpponentStarterPoints().values())/len(weeks), 2), style)
		sheet.write(row, column + 4, '-', style)
		sheet.write(row, column + 5, round(sum(medianStarterPoints.values())/len(weeks), 2), style)
		sheet.write(row, column + 6, '-', style)
		sheet.write(row, column + 7, '-', style)
		
		row = 0
		column = 0
	wb.save('output/bff-stats.xls')
	
def getWinsLossesString(winsLosses, winsLossesDoublerHeader={}):
	w = 0
	l = 0
	for week,wl in winsLosses.items():
		if wl == 'L':
			l = l + 1
		else:
			w = w + 1
	if winsLossesDoublerHeader:
		for week, wl in winsLossesDoublerHeader.items():
			if wl == 'L':
				l = l + 1
			else:
				w = w + 1	
	return str(w) + '-' + str(l)
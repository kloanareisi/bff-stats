import ast
import os
import requests

def downloadFiles(do_download, league_id, listOfFiles, logger):
	if do_download:
		if not os.path.exists('input'):
			os.makedirs('input')
		for f in listOfFiles:
			filename = f
			url = listOfFiles[f].replace('<league_id>', league_id)
			if filename.startswith('matchup_'):
				download_matchups(filename, url, logger)
			else:
				download_file(filename, url, logger)
	
def download_matchups(filename, url, logger):
	for i in range (1, 21):
		filename_week = filename.replace('<week_XX>', 'week_' + str(i))
		url_week = url.replace('<week_XX>', str(i))
		download_file(filename_week, url_week, logger)

def download_file(filename, url, logger):
	response = requests.get(url, allow_redirects=True)
	open('input/' + filename, 'w+').write(response.content.decode('utf-8'))
	if '[]' in open('input/' + filename, 'r').read():
		os.remove('input/' + filename)
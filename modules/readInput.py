import json
import datetime
from csv import reader
from modules.user import User
from modules.player import Player
from modules.matchup import Matchup

def readJsonFile(filename):
	with open('input/' + filename, 'r') as file:
		data = file.read()
	return json.loads(data)

def readUser(users):
	file = readJsonFile('users.json')
	for u in file:
		user = User(u['user_id'], str(u['display_name']))
		users[user.getId()] = user

def readRoster(users):
	file = readJsonFile('rosters.json')
	for r in file:
		users[r['owner_id']].setRosterId(r['roster_id'])
		users[r['owner_id']].setPlayers(r['players'])
		users[r['owner_id']].setWins(r['settings']['wins'])
		users[r['owner_id']].setTies(r['settings']['ties'])
		users[r['owner_id']].setLosses(r['settings']['losses'])
		users[r['owner_id']].setPpts(r['settings']['ppts'] + r['settings']['ppts_decimal']/100)
		users[r['owner_id']].setFpts(r['settings']['fpts'] + r['settings']['fpts_decimal']/100)
		users[r['owner_id']].setFptsAgainst(r['settings']['fpts_against'] + r['settings']['fpts_against_decimal']/100)
		
def readPlayer(players):
	file = readJsonFile('players.json')
	for k,p in file.items():
		player = Player(p['player_id'], 
			p['first_name'] + ' ' + p['last_name'],
			str(p['team']),
			p['fantasy_positions']
			)
		players[player.getId()] = player
		
def readWeeks(weeks, currentYear):
	week_list = {}
	with open ('input/games.csv', 'r') as readObject:
		csvReader = reader(readObject)
		for row in csvReader:
			if row[1] == str(currentYear):
				week = int(row[3])
				date = datetime.datetime.strptime(row[4][0:10], '%Y-%m-%d')
				if week not in week_list:
					week_list[week] = [date, date]
				else:
					if date < week_list[week][0]:
						week_list[week][0] = date
					elif date > week_list[week][1]:
						week_list[week][1] = date
	d = datetime.datetime.now()
	for k,w in week_list.items():
		# print(str(k) + ':' + str(w[0]) + ',' + str(w[1]))
		if d > w[1]:
			weeks.append(k)
			
def readMatchup(weeks, matchups):
	for w in weeks:
		file = readJsonFile('matchup_week_' + str(w) + '.json')
		for m in file:
			if m['matchup_id'] is not None:
				matchup = Matchup(w,
					int(m['matchup_id']),
					int(m['roster_id']),
					m['players'],
					m['players_points'],
					m['starters'],
					m['starters_points']
					)
				matchups.append(matchup)
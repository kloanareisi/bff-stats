import numpy
from modules.user import User
from modules.matchup import Matchup

def getUserIdByRosterId(users, rosterId):
	for k,u in users.items():
		if u.getRosterId() == rosterId:
			return k

def getPoints(users, matchups):
	for m in matchups:
		week = m.getWeek()
		userId = getUserIdByRosterId(users, m.getRosterId())
		playerPoints = 0.0
		for k,p in m.getPlayerPoints().items():
			playerPoints += p
		users[userId].addPlayerPoints(week, round(playerPoints, 2))
		users[userId].addStarterPoints(week, round(numpy.sum(m.getStarterPoints()), 2))

def getRanks(weeks, users, matchups):
	for w in weeks:
		rank = {}
		for k,u in users.items():
			if w in u.getStarterPoints():
				rank[u.getId()] = u.getStarterPoints()[w]
		rankSorted = sorted(rank.items(), key=lambda x: x[1], reverse=True)
		for i in range(0, len(rankSorted)):
			users[rankSorted[i][0]].addRank(w, i+1)

def getStarterPointsAgainst(weeks, users, matchups):
	# Gather opponents for each week
	for k,u in users.items():
		for w in weeks:
			for m in matchups:
				if m.getWeek() == w and m.getRosterId() == u.getRosterId():
					matchupId = m.getMatchupId()
					for m2 in matchups:
						if m2.getWeek() == w and m2.getMatchupId() == matchupId and m2.getRosterId() != m.getRosterId():
							users[k].addOpponents(w, m2.getRosterId())
	# Get starter points of each opponent per week
	for k,u in users.items():
		for w in weeks:
			for k2,u2 in users.items():
				if w in u.getOpponents():
					if u2.getRosterId() == u.getOpponents()[w]:
						u.addOpponentStarterPoints(w, u2.getStarterPoints()[w])
	# Get rank of each opponent per week
	for w in weeks:
		rank = {}
		for k,u in users.items():
			if w in u.getOpponentStarterPoints():
				rank[u.getOpponents()[w]] = u.getOpponentStarterPoints()[w]
		rankSorted = sorted(rank.items(), key=lambda x: x[1], reverse=True)
		for i in range(0, len(rankSorted)):
			for k,u in users.items():
				if w in u.getOpponents():
					if rankSorted[i][0] == u.getOpponents()[w]:
						u.addOpponentRank(w, i+1)
					
def getWinsLosses(weeks, users):
	for k,u in users.items():
		for w in weeks:
			if w in u.getOpponentStarterPoints():
				if u.getStarterPoints()[w] < u.getOpponentStarterPoints()[w]:
					u.addWinsLosses(w, 'L')
				else:
					u.addWinsLosses(w, 'W')
				
def getWinsLossesDoubleHeader(weeks, users, medianStarterPoints):
	for w in weeks:
		points = []
		for k,u in users.items():
			if w in u.getStarterPoints():
				points.append(u.getStarterPoints()[w])
		if points:
			medianStarterPoints[w] = round(numpy.median(points), 2)
	for k,u in users.items():
		for w in weeks:
			if w in u.getStarterPoints():
				if u.getStarterPoints()[w] < medianStarterPoints[w]:
					u.addWinsLossesDoubleHeader(w, 'L')
				else:
					u.addWinsLossesDoubleHeader(w, 'W')
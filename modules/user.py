class User:
	def getId(self):
		return self.id
		
	def getName(self):
		return self.name
		
	def getRosterId(self):
		return self.rosterId
		
	def setRosterId(self, rosterId):
		self.rosterId = rosterId
		
	def getWins(self):
		return self.wins
		
	def setWins(self, wins):
		self.wins = wins
		
	def getTies(self):
		return self.ties
		
	def setTies(self, ties):
		self.ties = ties
		
	def getLosses(self):
		return self.losses
		
	def setLosses(self, losses):
		self.losses = losses
		
	def getPpts(self):
		return self.ppts
		
	def setPpts(self, ppts):
		self.ppts = ppts
		
	def getFpts(self):
		return self.fpts
		
	def setFpts(self, fpts):
		self.fpts = fpts
		
	def getFptsAgainst(self):
		return self.fptsAgainst
		
	def setFptsAgainst(self, fptsAgainst):
		self.fptsAgainst = fptsAgainst
		
	def getPlayers(self):
		return self.players
		
	def setPlayers(self, players):
		self.players = players
		
	def getPlayerPoints(self):
		return self.playerPoints
		
	def addPlayerPoints(self, week, playerPoints):
		self.playerPoints[week] = playerPoints
		
	def getStarterPoints(self):
		return self.starterPoints
		
	def addStarterPoints(self, week, starterPoints):
		self.starterPoints[week] = starterPoints
		
	def getRank(self):
		return self.rank
	
	def addRank(self, week, rank):
		self.rank[week] = rank
	
	def getOpponents(self):
		return self.opponents
		
	def addOpponents(self, week, rosterId):
		self.opponents[week] = rosterId
	
	def getOpponentStarterPoints(self):
		return self.opponentStarterPoints
		
	def addOpponentStarterPoints(self, week, points):
		self.opponentStarterPoints[week] = points
		
	def getOpponentRank(self):
		return self.opponentRank
		
	def addOpponentRank(self, week, rank):
		self.opponentRank[week] = rank
		
	def getWinsLossesDoubleHeader(self):
		return self.winsLossesDoubleHeader
		
	def addWinsLossesDoubleHeader(self, week, winLoss):
		self.winsLossesDoubleHeader[week] = winLoss
		
	def getWinsLosses(self):
		return self.winsLosses
		
	def addWinsLosses(self,week,winLoss):
		self.winsLosses[week] = winLoss	
	
	def __init__(self, id, name):
		self.id = id
		self.name = name
		self.playerPoints = {}
		self.starterPoints = {}
		self.rank = {}
		self.opponents = {}
		self.opponentStarterPoints = {}
		self.opponentRank = {}
		self.winsLosses = {}
		self.winsLossesDoubleHeader = {}
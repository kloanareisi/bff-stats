class Matchup:
	def getWeek(self):
		return self.week
		
	def getMatchupId(self):
		return self.matchupId
		
	def getRosterId(self):
		return self.rosterId
		
	def getPlayer(self):
		return self.player
		
	def getPlayerPoints(self):
		return self.playerPoints
	
	def getStarter(self):
		return self.starter
		
	def getStarterPoints(self):
		return self.starterPoints
	
	def __init__(self, week, matchupId, rosterId, player, playerPoints, starter, starterPoints):
		self.week = week
		self.matchupId = matchupId
		self.rosterId = rosterId
		self.player = player
		self.playerPoints = playerPoints
		self.starter = starter
		self.starterPoints = starterPoints
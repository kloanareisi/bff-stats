class Player:
	def getId(self):
		return self.id
		
	def getName(self):
		return self.name
		
	def getTeam(self):
		return self.team
		
	def getFantasyPositions(self):
		return self.fantasyPositions
	
	def __init__(self, id, name, team, fantasyPositions):
		self.id = id
		self.name = name
		self.team = team
		self.fantasyPositions = fantasyPositions